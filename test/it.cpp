#include <mocha.hpp>
#include <stdexcept>

using namespace Mocha;

static auto it_test = describe("it", [](){
	it("runs the test", [](){
		int ran = 0;

		auto test = make_test(nullptr, "test test", [&](){
			ran++;
		});

		auto result = test->run();

		if(ran != 1) throw std::runtime_error("Did not run");
	});
});