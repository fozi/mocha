#include <mocha.hpp>
#include <stdexcept>

using namespace Mocha;

static Mocha::ITestGroup *describe_test = describe("describe", [](){
	it("creates a group", [](){
		auto test_group = make_group(nullptr, "test group");
		if(!test_group) throw std::runtime_error("No group");
		if(test_group->get_name() != "test group") throw std::runtime_error("unexpected group name");
	});
});