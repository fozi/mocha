#include <mocha.hpp>
#include <future>
#include <thread>
#include <iostream>

using namespace Mocha;
using namespace std;

static auto _ = describe("futures", []() {
	it("Accepts a function returning a future", []() {
		promise<error_code> promise;
		auto future = promise.get_future();

		cout << "starting thread" << endl;

		thread thread([](auto promise) {
			cout << "in thread" << endl;
			this_thread::sleep_for(1s);
			promise.set_value(error_code());
			cout << "thread done" << endl;
		}, move(promise));

		thread.detach();

		cout << "exiting test function" << endl;
		return future;
	});
});
