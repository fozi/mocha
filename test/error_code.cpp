#include <mocha.hpp>
#include <system_error>

using namespace Mocha;
using namespace std;

static auto b = describe("bool", []() {
	it("accepts a test function returning a bool", []() -> bool {
		return false;
	});
});

static auto s = describe("string", []() {
	it("accepts a test function returning a string", []() {
		return nullptr;
	});
});

static auto e = describe("error_code", []() {
	it("accepts a function returning a std::error_code", []() {
		return error_code();
	});

	struct Test { const char * name; error_code result; };
	for (auto &test : {
		Test { "succeed", error_code() },
		Test { "fail", make_error_code(errc::file_exists) },
	}) it(test.name, [test]() {
		auto t = make_test(nullptr, test.name, [test]() { return test.result; });
		try {
			auto result = t->run();
			if(test.result && result.passing) throw runtime_error("Exception expected");
		} catch(const std::error_code err) {
			if(err != test.result) throw runtime_error("Unexpected result thrown");
			if(!err) throw runtime_error("Unexpected throw");
		}
	});
});
