# Mocha for C++

© 2022 [Fozi](mailto:mocha@fozi.ca)

This is a test framework for C++ that looks suspiciously like [Mocha for node.js](https://mochajs.org). It organizes and runs your tests.

It is not an assertion library (like <assert.h> or Chai) so it does not include any type of functions for checking assertions.

*Note:* This is still alpha and work in progress. No guarantees. The `master` branch is in development.

## Building the tests

This library is written in standard modern C++17. Platform specific sections are `#ifdef`ed off. Any conformant C++17 compiler should be able to compile it.

It is also deliberately placed in only two files (.cpp and .hpp) and there are no build files. It is out of scope of this library to tell you what build system you should be using.

So, to use it, all you have to do is to:

- Include `mocha.hpp` in all your test files
- Compile and link any number of files together with `mocha.cpp` to build an executable that will run the tests
- Run the executable to run the tests

### Example compiler usage

Given files `foo.cpp` and `bar.cpp` that both have a set of tests and include `mocha.hpp`, and the `mocha.cpp` file, you can compile it to a test executable and run it:

#### Microsoft Visual C++

```console
cl mocha.cpp foo.cpp bar.cpp -O2
mocha
```

#### GCC
```console
g++ mocha.cpp foo.cpp bar.cpp -o mocha -O3
./mocha
```

#### Makefile

This example makefile just builds everything in a folder (e.g. `test`) as a test and runs it:

```Makefile
run: mocha
	./$^

mocha: $(wildcard *.cpp)
	$(CXX) $^ -o $@ -O3

clean:
	rm mocha
```

So you would run your tests by

```console
cd test
make
```

### Bring Your Own Main

By default `mocha.cpp` defines a `main` function. You can make it not do that by defining `MOCHA_NO_MAIN`.

The included `main` is very simple:

```cpp
int main() {
	return Mocha::Run();
}
```

## Writing the tests

If you ever used the Mocha.js library you will find yourself at home.

From a high level, you declare test groups or topics with `describe` and the actual tests with `it`.

### An Example

`foo.hpp`, the code to be tested:
```cpp
#pragma once

struct Foo {
	int x;
	Foo(int x) : x(x) {}
	int add(int y) const { return x+y; }
};
```

`test/foo.cpp`, the test code:
```cpp
#include "mocha.hpp"

// include testee
#include "foo.hpp"

// include your favourite assertion library instead
#include <stdexcept>

// tests
using namespace Mocha;

auto footests = describe("FOO", [](){
	it("can be constructed", [](){
		Foo foo = 5;
		if(foo.x != 5) throw std::runtime_error("foo.x != 5");
	});

	// describe can be nested
	describe("Foo::add can add numbers", [](){
		// C++ has types, so we need this :(
		struct Test {
			const char* name;
			int x, y, expected;
		};

		// data driven testing
		for(auto &test : { Test
			{ "one", 1, 2, 3},
			{ "other", 4, 5, 9},
			{ "fails", 3, 3, 3},
		}) it(test.name, [](auto &test) {
			Foo foo = test.x;
			if(foo.add(test.y) != test.expected) throw std::runtime_error("unexpected result");
		}, test);
	});
});

```
*Note:** `footests` above is needed but not used because in C++ you can't execute functions on the namespace level without a declaration.

When [running this](#building-the-tests) you should get something like:

```
❯ ./foo
  FOO
    √ can be constructed
    Foo::add can add numbers
      √ one
      √ other
      1) fails

  3 passing (0ms)
  1 failing

  1) FOO
        Foo::add can add numbers
          fails:
      unexpected result
```
with an error result of `1`.

## TODO:

These are things that might appear sometime in the future.

- futures
- timeouts
- hooks
- output stream control
- support for serial output
- templated tests
- static assertion tests
- Stack trace?
- parallel mode
- command line arguments
- documentation
- 100% test coverage
