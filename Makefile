CPPFLAGS=-std=c++17 -Iinc -pthread

test: build/mocha
	@./$^

build/mocha: src/mocha.cpp $(wildcard test/*.cpp)
	@mkdir -p build
	@$(CXX) $(CPPFLAGS) $^ -o $@ -O3

clean:
	@rm -rf build
