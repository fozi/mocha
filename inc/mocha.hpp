#pragma once

#include <string>
#include <vector>
#include <functional>
#include <initializer_list>
#include <utility>
#include <future>
#include <system_error>
#include <chrono>

namespace Mocha {

namespace detail {

	template<class R>
	struct TestRunner {
	public:
		static void run(const std::function<R()> &fun) {
			if constexpr (std::is_same<void, R>::value) {
				fun();
			} else {
				auto result = fun();
				if (result) throw result;
			}
		}
	};

	template<class R>
	struct TestRunner<std::future<R>> {
	public:
		static void run(const std::function<std::future<R>()> &fun) {
			std::future<R> res = fun();
			if constexpr (std::is_same<void, R>::value) {
				res.get();
			} else {
				auto result = res.get();
				if (result) throw result;
			}
		}
	};

	class ITestHolder {
	public:
		virtual void run() = 0;
		virtual bool has_test() const = 0;
	};

	template<class R>
	class TestHolder : public ITestHolder, public std::function<R()> {
	public:
		template<class F>
		TestHolder(F && func) : std::function<R()>(std::forward<F>(func)) {}

		void run() override {
			TestRunner<R>::run(*this);
		}

		bool has_test() const {
			return (bool)*this;
		}
	};
}

struct Report {
	unsigned total, passing, skipped;
	std::chrono::microseconds duration;
};

class ITestGroup;
class IReporter;

class ITest {
public:
	virtual Report run(bool only_only = false, bool skip = false) = 0;
	virtual void skip() = 0;
	virtual void only() = 0;
	virtual std::string get_name() const = 0;
	virtual ITestGroup* get_group() const = 0;
	virtual bool has_only() const = 0;
	virtual IReporter *get_reporter() const = 0;
};

class ITestGroup : public ITest {
public:
	virtual ITest* add(ITest*) = 0;
};

ITestGroup* current_group();
ITestGroup* root_group();

ITestGroup* make_group(ITestGroup* group, const char* name);

namespace detail {
	ITest* make_test(ITestGroup* group, std::string name, ITestHolder * test);
}

template<class Func>
ITest* make_test(ITestGroup* group, std::string name, Func && func) {
	return detail::make_test(group, move(name), new detail::TestHolder<decltype(func())>(std::forward<Func>(func)));
}

class Describe {
public:
	ITestGroup* operator() (std::string name, std::function<void()> func);
	ITestGroup* only(std::string name, std::function<void()> func);
	ITestGroup* skip(std::string name, std::function<void()> func);
};

extern Describe describe;

class It {
public:
	template<class Callable>
	ITest* operator() (std::string name, Callable && func) {
		return current_group()->add(make_test(current_group(), std::move(name), std::forward<Callable>(func)));
	}

	template<class Callable, class Arg1, class... Args>
	ITest* operator() (std::string name, Callable func, Arg1 arg1, Args... args) {
		return (*this)(name, std::bind(func, arg1, args...));
	}

	template<class Callable>
	ITest* only(std::string name, Callable && func) {
		auto test = (*this)(name, std::forward(func));
		test->only();
		return test;
	}

	template<class Callable, class Arg1, class... Args>
	ITest* only(std::string name, Callable func, Arg1 arg1, Args... args) {
		return only(name, std::bind(func, arg1, args...));
	}

	ITest* operator() (std::string name) {
		return (*this)(std::move(name), std::function<void()>());
	}

	template<class Callable>
	ITest* skip(std::string name, Callable && func) {
		auto test = (*this)(name, std::move(func));
		test->skip();
		return test;
	}

	template<class Callable, class Arg1, class... Args>
	ITest* skip(std::string name, Callable func, Arg1 arg1, Args... args) {
		return skip(name, std::bind(func, arg1, args...));
	}
};

extern It it;

template<class Callable, class... Args>
ITest* xit(std::string name, Callable, Args...) {
	return it(name);
}

int Run();

}
