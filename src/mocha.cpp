#include "mocha.hpp"

#ifdef _WIN32
#include <Windows.h>
#endif

#include <iostream>
#include <algorithm>

namespace Mocha {

namespace colors {
	static auto const black = "\u001b[30m";
	static auto const red = "\u001b[31m";
	static auto const green = "\u001b[32m";
	static auto const yellow = "\u001b[33m";
	static auto const blue = "\u001b[34m";
	static auto const magenta = "\u001b[35m";
	static auto const cyan = "\u001b[36m";
	static auto const white = "\u001b[37m";
	static auto const bright_black = "\u001b[30;1m";
	static auto const bright_red = "\u001b[31;1m";
	static auto const bright_green = "\u001b[32;1m";
	static auto const bright_yellow = "\u001b[33;1m";
	static auto const bright_blue = "\u001b[34;1m";
	static auto const bright_magenta = "\u001b[35;1m";
	static auto const bright_cyan = "\u001b[36;1m";
	static auto const bright_white = "\u001b[37;1m";
	static auto const reset = "\u001b[0m";
}

using namespace std;

template<class T>
class Reset {
public:
	Reset(T &var, T new_value, T reset_value) : var(var), value(reset_value) {
		var = new_value;
	}

	~Reset() {
		var = value;
	}
private:
	T &var;
	T value;
};

struct Error {
	ITest * test;
	string message;
	string stack;
};

class IReporter {
public:
	virtual void report_success(ITest *test, const Report &report, std::chrono::milliseconds slow) = 0;
	virtual void report_skipped(ITest *test) = 0;
	virtual void report_error(ITest *test, const std::string &message, const std::string &stack = "") = 0;

	virtual void print_title(ITestGroup *group) = 0;
};

class Reporter : public IReporter {
public:
	explicit Reporter(ostream &out, bool color = true) : out(out), usecolor(color) {}

	void report_success(ITest *test, const Report &report, std::chrono::milliseconds slow) override {
		if(!test) return;

		out << color(colors::bright_black) << indent(test->get_group()) << color(colors::green) << "√ " << color(colors::bright_black) << test->get_name();
		if(report.duration > (slow/2)) {
			out << color(report.duration > slow ? colors::red : colors::yellow);
			out << " (" << (double)report.duration.count()/1000 << "ms)";
		}
		out << color(colors::reset) << endl;
	}

	void report_skipped(ITest *test) override {
		if(!test) return;

		out << color(colors::cyan) << indent(test->get_group()) << "- " << test->get_name() << color(colors::reset) << endl;
	}

	void report_error(ITest *test, const string &message, const string &stack) override {
		if(!test) return;

		auto err_n = record_error(test, message, stack);
		out << color(colors::red) << indent(test->get_group()) << err_n << ") " << test->get_name() << color(colors::reset) << endl;
	}

	void print_title(ITestGroup *group) override {
		if(!group) return;
		auto name = group->get_name();
		auto parent = group->get_group();
		if(!name.empty()) out << color(colors::white) << (parent ? indent(parent) : "") << name << color(colors::reset) << endl;
	}

	void print_errors() {
		if(errors.size()) {
			unsigned n = 0;
			for(auto error : errors) {
				out << endl;
				print_error_title(++n, error.test);
				out << color(colors::red) << indent(error.message, "      ") << color(colors::reset) << endl;
				if(!error.stack.empty()) out << color(colors::bright_black) << indent(error.stack, "       ") << color(colors::reset) << endl;
			}
		}
	}

	void print_notests() {
		out << endl << color(colors::red) << "Error: No tests found" << color(colors::reset) << endl;
	}

	void print_summary(const Report &report) {
		auto failing = report.total - report.passing - report.skipped;
		out << endl << color(colors::green) << "  " << report.passing << " passing" << color(colors::bright_black) << " (" << (double)report.duration.count()/1000 << "ms)" << color(colors::reset) << endl;
		if(report.skipped) out << color(colors::cyan) << "  " << report.skipped << " pending" << color(colors::reset) << endl;
		if(failing) out << color(colors::red) << "  " << failing << " failing" << color(colors::reset) << endl;
	}

private:
	std::ostream &out;
	vector<Error> errors;
	bool usecolor;

	const char* color(const char* color) {
		if(usecolor) return color;
		return "";
	}

	size_t record_error(ITest * test, string message, string stack = "") {
		errors.push_back(Error { test, message, stack });
		return errors.size();
	}

	static int level(ITest *test) {
		if(test) return level(test->get_group()) + 1;
		return 0;
	}

	static string indent(ITest *test) {
		return string(level(test)*2, ' ');
	}

	static vector<string> path(ITest * test) {
		auto *parent = test->get_group();
		auto name = test->get_name();
		auto result = parent ? path(parent) : vector<string>();
		if(!name.empty()) result.push_back(name);
		return result;
	}

	static string indent(string message, string indent) {
		for(auto pos = message.find('\n'); pos != string::npos; pos = message.find('\n', pos)) {
			message.insert(pos+1, indent);
			pos += indent.size() + 1;
		}
		return indent + message;
	}

	void print_error_title(unsigned n, ITest *test) {
		out << color(colors::white) << "  " << n << ") ";
		n = 0;
		for(auto name : path(test)) {
			if(n++) {
				out << endl << "    ";
				for(auto x = n; x; --x) out << "  ";
			}
			out << name;
		}
		out << ":" << color(colors::reset) << endl;
	}
};

class skip_test {};

template<class IF>
class TestBase : public IF {
public:
	ITestGroup * const parent;
	IReporter * reporter;
	const string name;
	bool onlythis, skipthis;

public:
	TestBase(ITestGroup *parent, string name) : parent(parent), name(move(name)), onlythis(false), skipthis(false), reporter(nullptr) {}
	string get_name() const override { return name; }
	ITestGroup* get_group() const override { return parent; }
	void skip() override { skipthis = true; }
	void only() override { onlythis = true; }
	bool has_only() const override { return onlythis; }
	IReporter* get_reporter() const { return reporter ? reporter : parent ? parent->get_reporter() : nullptr; }
	void set_reporter(IReporter *reporter = nullptr) { this->reporter = reporter; }
};

class Test : public TestBase<ITest> {
public:
	Test(ITestGroup *parent, string name, detail::ITestHolder *test) : TestBase(parent, move(name)), test(test), slow(75), running(false) {}

	~Test() {
		delete test;
	}

private:
	detail::ITestHolder *test;
	bool running;

public:
	chrono::milliseconds slow;

public:
	void skip() override {
		TestBase<ITest>::skip();
		if(running) throw skip_test();
	}

	Report run(bool only_only = false, bool skip = false) override;
};

class TestGroup : public TestBase<ITestGroup> {
private:
	vector<ITest*> tests;

public:
	TestGroup() : TestBase(nullptr, "") {}
	TestGroup(ITestGroup *parent, string name) : TestBase(parent, move(name)) {}
	TestGroup(ITestGroup *parent, string name, initializer_list<ITest*> tests) : TestBase(parent, move(name)), tests(tests) {}

	Report run(bool only_only = false, bool skip = false) override;

	using TestBase::get_name;
	using TestBase::get_group;

	ITest* add(ITest *test) override {
		tests.push_back(test);
		return test;
	}

	bool has_only() const override {
		return onlythis || any_of(begin(tests), end(tests), [](ITest *test) {
			return test->has_only();
		});
	}
};


Report Test::run(bool only_only, bool skip) {
	Report report = { (only_only && !onlythis) ? 0u : 1 };
	using namespace chrono;
	if(report.total) {
		if(!skip && !skipthis && test && test->has_test()) {
			try {
				Reset<bool> reset(running, true, false);
				auto start = high_resolution_clock::now();
				test->run();
				report.duration = duration_cast<microseconds>(high_resolution_clock::now() - start);
				report.passing = 1;
				if(auto reporter = get_reporter()) reporter->report_success(this, report, slow);
				return report;
			} catch(const skip_test&) {
				goto skipped;
			} catch(const exception &e) {
				if(auto reporter = get_reporter()) reporter->report_error(this, e.what());
			} catch(const bool &failed) {
				if(auto reporter = get_reporter()) reporter->report_error(this, string("Test returned ") + (failed?"true":"false"));
			} catch(const char *msg) {
				if(auto reporter = get_reporter()) reporter->report_error(this, string("Test failed: ") + msg);
			} catch(const std::error_code &e) {
				if(auto reporter = get_reporter()) reporter->report_error(this, string("Test failed with error: ") + e.message());
			} catch(...) {
				if(auto reporter = get_reporter()) reporter->report_error(this, "Unknown exception");
			}
		} else {
skipped:
			report.skipped = 1;
			if(auto reporter = get_reporter()) reporter->report_skipped(this);
		}
	}
	return report;
}

Report TestGroup::run(bool only_only, bool skip) {
	Report report = { 0 };

	if(only_only && onlythis && none_of(begin(tests), end(tests), [](ITest *test) {
		return test->has_only();
	})) {
		// this group is .only but none of our children is .only, so all of them should be executed
		only_only = false;
	} else if(only_only && !this->has_only()) {
		return report;
	}

	if(auto reporter = get_reporter()) reporter->print_title(this);
	for(auto &test : tests) {
		auto testreport = test->run(only_only, skip || skipthis);
		report.total += testreport.total;
		report.passing += testreport.passing;
		report.skipped += testreport.skipped;
		report.duration += testreport.duration;
	}

	return report;
}

static TestGroup* root() {
	static TestGroup root;
	return &root;
}

static ITestGroup** cg() {
	static thread_local ITestGroup *current_group = root();
	return &current_group;
}

ITestGroup* current_group() {
	return *cg();
}

ITestGroup* root_group() {
	return root();
}

ITestGroup* make_group(ITestGroup* group, const char* name) {
	return new TestGroup(group, name);
}

namespace detail {
	ITest* make_test(ITestGroup* group, string name, ITestHolder * test) {
		return new Test(group, move(name), test);
	}
}

ITestGroup* set_current_group(ITestGroup *new_current) {
	ITestGroup *prev = *cg();
	*cg() = new_current;
	return prev;
}

ITestGroup* Describe::operator() (string name, function<void()> func) {
	auto *parent = current_group();
	auto *current = new TestGroup(parent, name);
	set_current_group(current);
	if(parent) parent->add(current);

	try {
		func();
	} catch(...) {
		cerr << "Exception in setup" << endl;
	}

	set_current_group(parent);
	return current;
}

ITestGroup* Describe::only(std::string name, std::function<void()> func) {
	auto *group = (*this)(name, move(func));
	group->only();
	return group;
}

ITestGroup* Describe::skip(std::string name, std::function<void()> func) {
	auto *group = (*this)(name, move(func));
	group->skip();
	return group;
}

Describe describe;
It it;

int Run() {
#ifdef _WIN32
	SetConsoleOutputCP(CP_UTF8);
	{
		DWORD mode;
		auto console = GetStdHandle(STD_OUTPUT_HANDLE);
		if(GetConsoleMode(console, &mode)) SetConsoleMode(console, mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	}
#endif

	auto reporter = new Reporter(cout);
	root()->set_reporter(reporter);

	auto report = root()->run(root()->has_only());
	if(!report.total) {
		reporter->print_notests();
		return 1;
	}

	reporter->print_summary(report);
	reporter->print_errors();

	auto failing = report.total - report.passing - report.skipped;
	return failing > 127 ? 127 : failing;
}

}

#ifndef MOCHA_NO_MAIN
int main(int argc, char** argv) {
	return Mocha::Run();
}
#endif
